library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity BFP_Conv_2D is
    Generic (
		constant IM_WIDTH  : positive:=80;
		constant DATA_WIDTH	: positive :=16;
		constant MAN_WIDTH : positive := 11;
		constant KERNEL_WIDTH :positive:=3
	);
	port(
        clk_i   : in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		C       : in t_Kernel(2 downto 0);
		data_valid_i : in std_logic;
		valid_out: out std_logic;
		Num_back : out std_logic_vector(15 downto 0);
		Max : out std_logic_vector(4 downto 0)

		
		
		);
end BFP_Conv_2D;

architecture Behavioral of BFP_Conv_2D is
component Construct is
    Generic (
		
		constant DATA_WIDTH	: positive := MAN_WIDTH
	);
    port(
        clk_i : in std_logic;
        b_sign : in std_logic;
        In_exp : in std_logic_vector(4 downto 0);
        F_exp  : in std_logic_vector(4 downto 0);
        Mantissa : in std_logic_vector(23 downto 0);
        Format : out std_logic_vector(15 downto 0);
        Max : out std_logic_vector(4 downto 0)
     
        );
        

end component; 
component BRAM1 is
 Generic (
		
		constant DATA_WIDTH	: positive := MAN_WIDTH
	);
  Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		Zero_flag : in boolean;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		flag_left : in boolean; 
		data_out : out  slv16bit_array_t(0 to KERNEL_WIDTH-1)
                                 
	);  
end component;
component BRAM2 is
 Generic (
		
		constant DATA_WIDTH	: positive := MAN_WIDTH
	);
  Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		flag_left : in boolean;  -- for left limits
		data_out : out  slv16bit_array_t(0 to KERNEL_WIDTh-1)
                                 
	);  
end component;

component conv_unit is
 Generic (   
				 constant DATA_WIDTH 		: positive := DATA_WIDTH	
				 );
				 
    Port (clk : in std_logic;
          data : in out_data (2 downto 0);
          valid_in : in std_logic;
          B       : in out_data(2 downto 0);
          Q : out  STD_LOGIC_VECTOR (23 downto 0);
          multiply : out mult_data(2 downto 0);
          K_sign : out std_logic
           );
end component;
component Control is
   Generic (
		constant RAM_DEPTH  : positive := IM_WIDTH;
		constant RAM_WIDTH	: positive := MAN_WIDTH
	);
	port(
        clk_i   : in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		wr_en : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (RAM_WIDTH - 1 downto 0);
		datas : out  slv16bit_array_t(0 to KERNEL_WIDTH-1);
		valid : out slv_array_t(0 to 1)
		
		);
end component;
component Flag_Generator is
 Generic (
		
		constant IM_WIDTH	: positive := IM_WIDTH
	);
 port(
        clk_i   : in  STD_LOGIC;
        valid_in : in std_logic;
		Zero_flag : out boolean;
		k1 : out boolean;
		valid_out : out std_logic;
		valid_conv: out std_logic

		);
end component;
   
signal datak     :  out_data(0 to 2);
signal datas     :  slv16bit_array_t(0 to KERNEL_WIDTH-1);
signal datas1     :  slv16bit_array_t(0 to KERNEL_WIDTH-1);
signal valid     :  slv_array_t(0 to 1);
signal valid1     :  slv_array_t(0 to 1);
signal k1 : boolean;
signal k2 : boolean;
signal Sum1 : std_logic_vector(23 downto 0);
signal B : out_data(2 downto 0);
signal F_exp,I_exp : std_logic_vector(4 downto 0);
signal valid_conv : std_logic;
signal Zero_flag : boolean := false;
signal Mantissa :   STD_LOGIC_VECTOR (MAN_WIDTH - 1 downto 0);
signal valids : std_logic;
signal sign_buff : std_logic;
begin
 process(clk_i)
    begin
        if (rising_edge(clk_i)) then
            Mantissa <= data_i(15) & data_i(9 downto 0);
            I_exp <= data_i(14 downto 10);
            F_exp <= C(0)(0)(14 downto 10);
            valid <=valid1;
            k2 <= k1;
            for i in 0 to KERNEL_WIDTH-1 loop
                for j in 0 to KERNEL_WIDTH-1 loop
                    B(i)(j) <= C(i)(j)(15) & C(i)(j)(9 downto 0);
                end loop;
            end loop;
            --valids <=valid1(0);
           -- datas <=datas1;
        end if;
    end process;
       
 MAIN_RAM : Control port map(clk_i=>clk_i,rst_i=>rst_i,wr_en=>data_valid_i,data_i=>Mantissa,datas=>datas,valid=>valid1);
 Gen_Flags: Flag_Generator port map(clk_i=>clk_i,valid_in=>valid(0),Zero_flag=>Zero_flag,k1=>k1,valid_out=>valid_out,valid_conv=>valid_conv);
 Gen_BRAM : for i in 0 to KERNEL_WIDTH-1 generate
  Gen_first : if i =0 generate
      BRAM : BRAM1 port map(clk_i=>clk_i,rst_i=>rst_i,Zero_flag=>Zero_flag,data_valid_i=>valid(0),data_i=>datas(i),flag_left=>k2,data_out=>datak(i));
  end  generate;
  Gen_others: if i =1 generate
      BRAMk: BRAM2 port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid(0),data_i=>datas(i),flag_left=>k2,data_out=>datak(i));
  end generate;
  Gen_third: if i =2 generate
      BRAM3: BRAM2 port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid(1),data_i=>datas(i),flag_left=>k2,data_out=>datak(i));
  end generate;
 end generate;

 CONVO  : conv_unit port map(clk=>clk_i,data=>datak,B=>B,valid_in=>valid_conv,Q=>Sum1,K_sign=>sign_buff );
 Make_it_back : Construct port map(clk_i=>clk_i,b_sign=>sign_buff,In_exp => I_exp,F_exp =>F_exp,Mantissa=> Sum1,Format=>Num_back,Max=>Max);      
end Behavioral;
