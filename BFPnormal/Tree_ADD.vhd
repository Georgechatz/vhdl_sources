library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;
entity Tree_ADD is
     Generic (
		
		constant DATA_WIDTH	: positive 
	);
	port
	(   clk: in std_logic;
		data : in mult_data(2 downto 0);
		signs : in sign_array (2 downto 0);
		result : out std_logic_vector (DATA_WIDTH - 1 downto 0);
		ssign : out std_logic
	);
	
end entity;

architecture rtl of Tree_ADD is
signal news : data24(2 downto 0);
signal temp : data24(2 downto 0);
signal neg  : data24(2 downto 0);
signal result_buff : std_logic_vector(DATA_WIDTH-1 downto 0);
signal temp_b :  std_logic_vector(DATA_WIDTH-1 downto 0);
	-- Declare registers to hold intermediate sums
signal sum1, sum2, sum3,sum4,buff1,sum5,sum6,buff2,sum7,buff3 : std_logic_vector (DATA_WIDTH-1 downto 0);
signal news1,news2,news3,news4,news5,news6,news7,sign_buff1,sign_buff2,sign_buff3 : std_logic;
begin
	process(clk)
	   begin
	      if rising_edge(clk) then
	        for i in  0 to 2 loop
	           for j in  0 to 2 loop
	               if (signs(i)(j) = '1') then
	                   news(i)(j) <= data(i)(j)(19)&data(i)(j)(19)&data(i)(j)(19)&data(i)(j)(19)&data(i)(j);
	                   temp(i)(j)<= not(news(i)(j));
	                   neg(i)(j) <= std_logic_vector(unsigned(temp(i)(j)+1));
	               else 
	                   news(i)(j) <= data(i)(j)(19)&data(i)(j)(19)&data(i)(j)(19)&data(i)(j)(19)&data(i)(j);
	                   temp(i)(j)<= news(i)(j);
	                   neg(i)(j) <= temp(i)(j);
	               end if;
	           end loop;
	        end loop;
	        
	        sum1 <= neg(0)(0)+neg(0)(1);
			sum2 <= neg(0)(2)+neg(1)(0);
			sum3 <= neg(1)(1)+neg(1)(2);
			sum4 <= neg(2)(0)+neg(2)(1);
			buff1<=neg(2)(2);
			sum5 <= sum1 +sum2;
			sum6 <= sum3 +sum4;
			buff2<=buff1;
			sum7 <= sum5 +sum6;
			buff3<=buff2;
			result_buff <= sum7 +buff3;
            if (result_buff <0 ) then
                temp_b <= not(result_buff);
                result <= std_logic_vector(unsigned(temp_b+1));
                ssign <= '1';
            else
                temp_b <=result_buff;
                result <= temp_b;
                ssign <='0';
            end if;
			
	    end if;
	   end process;
end rtl;