library ieee;
use ieee.std_logic_1164.all;

package mypack is
  type slv16bit_array_t is array (natural range <>) of std_logic_vector(10 downto 0);
  type sl16BIT is array (natural range <>) of std_logic_vector(15 downto 0);
  type Kernel_l6  is array (natural range <>) of std_logic_vector(15 downto 0);
  type t_Kernel is array (natural range <>) of  Kernel_l6(2 downto 0);
  type slv10bit_array_t is array (natural range <>) of std_logic_vector(10 downto 0);
  type slv20bit_array_t is array (natural range <>) of std_logic_vector(19 downto 0);
  type slv24bit_array_t is array (natural range <>) of std_logic_vector(23 downto 0);
  type new_multi is array (natural range <>) of slv10bit_array_t(2 downto 0);
  --type slv10bit_array_t is array (natural range <>) of std_logic_vector(9 downto 0);
  type slv32bit_array_t is array (natural range <>) of std_logic_vector(19 downto 0);
  type slv16_array_t is array (natural range <>) of std_logic_vector(17 downto 0);
  type slv8_array_t is array (natural range <>) of std_logic_vector(8 downto 0);
  type out_data is array (natural range <>) of slv16bit_array_t(2 downto 0);
  type Kern_32 is array (natural range <>) of t_Kernel(2 downto 0);
  type Max_arr_t is array (natural range <>) of std_logic_vector(4 downto 0);
  type slv_array_t is array (natural range <>) of std_logic;
  type sign_array is array (natural range <>) of slv_array_t(2 downto 0);
  type data24 is array (natural range <>) of slv24bit_array_t(2 downto 0);
  type mult_data is array (natural range <>) of slv32bit_array_t(2 downto 0);
end package;

package body mypack is
end package body;