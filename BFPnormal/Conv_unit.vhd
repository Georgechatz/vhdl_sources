library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;

entity conv_unit is
	 Generic (   
				 constant DATA_WIDTH 		: positive ;
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 
    Port ( clk : in std_logic;
           data : in out_data (2 downto 0);
           B       : in out_data(2 downto 0);
           valid_in : in std_logic;
           Q : out  STD_LOGIC_VECTOR (23 downto 0);
           multiply : out mult_data(2 downto 0);
           K_sign : out std_logic
            
           );
end conv_unit;
architecture beh of conv_unit is

component Tree_ADD is
    generic( constant DATA_WIDTH :positive := 24 );
	port
	(   clk  : in std_logic;
	    signs : in sign_array (2 downto 0);
	    data : in mult_data(2 downto 0);
		result : out std_logic_vector (23 downto 0);
		ssign : out std_logic
	);
	
end component;
    signal n_multi : new_multi(2 downto 0);
    signal multi : mult_data(2 downto 0);
    signal round : std_logic;
    --signal sign : std_logic;
    signal flsb  : std_logic;
    signal sign : sign_array(2 downto 0);
    signal sticky_f  : std_logic;
    signal sticky : std_logic_vector(8 downto 0);
    type kernel_3x3 is array(0 to 2,0 to 2) of std_logic_vector(10 downto 0);
	--constant B : kernel_3x3 := (("10001000000","10001000000","10001000000"),
	 --                           ("10001000000","01000000000","10001000000"),
	 --                           ("10001000000","10001000000","10001000000"));

	
	begin
	  process(clk)
	    begin
	       if rising_edge(clk) then   
	           --if valid_in ='1' then 
                    for i in 0 to KERNEL_WIDTH-1 loop
                        for j in 0 to KERNEL_WIDTH-1 loop
                            multi(i)(j)<=data(KERNEL_WIDTH-1-i)(j)(9 downto 0)*B(i)(j)(9 downto 0);
                            sign(i)(j) <= data(KERNEL_WIDTH-1-i)(j)(10) xor B(i)(j)(10);
                        end loop;
                    end loop;
                    multiply <= multi;
               
           end if;
	   end process;

	
	   TREE : Tree_ADD port map(clk=>clk,signs=>sign,data=>multi,result=>Q,ssign=>K_sign);
	
	
		  


end beh;