library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_unSIGNED.ALL;
--use iee.std_logic_unsigned.all
library work;
use work.mypack.all;
entity Construct is
     Generic (
		
		constant DATA_WIDTH	: positive 
	);
	port
	(   clk_i: in std_logic;
	    b_sign : in std_logic;
		In_exp : in std_logic_vector(4 downto 0);
		F_exp : in std_logic_vector(4 downto 0);
		Mantissa : in std_logic_vector(23 downto 0);
		Format : out std_logic_vector(15 downto 0);
		Max : out std_logic_vector(4 downto 0)
	);
	
end entity;
architecture beh of Construct is
signal New_exp_buff : std_logic_vector(4 downto 0);
signal New_exp,New_exp1 : std_logic_vector(4 downto 0);
signal New_man,New_man1 : std_logic_vector(9 downto 0);
signal position : integer := 0;
signal Mantissa1 : std_logic_vector(23 downto 0);
signal sign_buff1,sign_buff2,sign_buff3 : std_logic;
signal shifter: integer;
signal temp_max : std_logic_vector(4 downto 0) :="00000" ;
signal flag : boolean;
begin
    process(clk_i)
        begin
            if rising_edge(clk_i) then
                sign_buff1 <= b_sign;
                sign_buff2 <= sign_buff1;
                sign_buff3 <= sign_buff2;
               -- Mantissa1<=Mantissa;
               -- New_exp_buff <= In_exp-15+F_exp-15;
                L1 :for i in 23 downto 0 loop
                    if Mantissa(i)='1' then
                        position <= i;
                        flag <= true;
                        exit L1;
                    end if;
                end loop L1;
                Mantissa1<=Mantissa;
                if flag = true then
                    if position >= 10 then
                        New_man1 <= Mantissa1(position-1 downto position-10);
                    else 
                        New_man1 <= (others=>'0');
                    end if;
                    if position <=20 then
                    --shifter <= 21 - position;
                    --New_exp <="XXXXX";
                        New_exp1 <= In_exp-15+F_exp - std_logic_vector(to_unsigned(20-position,5));
               
                    elsif position >=20 then
                        --shifter <= 23 - position;
                        New_exp1 <= In_exp-15+F_exp + std_logic_vector(to_unsigned(23-position,5));
                    end if;
                    --temp_max <=New_exp1;
                    --New_exp<=New_exp1;
                    --New_man<=New_man1;
                    Format(15) <=sign_buff3;
                    Format(14 downto 10) <= New_exp1;
                    Format(9 downto 0)<=New_man1;
                else
                    New_man <= (others=>'0');
                end if;
                if New_exp1 > temp_max then
                    temp_max <=New_exp1;
                    Max <=New_exp1;
                end if;
              
               
            
                    
        
        
            end if;
        end process;
end beh;