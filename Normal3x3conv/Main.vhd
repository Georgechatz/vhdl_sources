library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity Conv2DApprox is
    Generic (
		constant IM_WIDTH  : positive:=80;
		constant DATA_WIDTH	: positive :=16;
		constant KERNEL_WIDTH :positive:=3
	);
	port(
        clk_i   : in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		B       : in out_data(2 downto 0);
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		data_valid_i : in std_logic;
		Sum : out  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		valid_out: out std_logic
		
		
		);
end Conv2DApprox;

architecture Behavioral of Main is

component BRAM1 is
 Generic (
		
		constant DATA_WIDTH	: positive := DATA_WIDTH
	);
  Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		Zero_flag : in boolean;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		flag_left : in boolean; 
		data_out : out  slv16bit_array_t(0 to KERNEL_WIDTH-1)
                                 
	);  
end component;
component BRAM2 is
 Generic (
		
		constant DATA_WIDTH	: positive := DATA_WIDTH
	);
  Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		flag_left : in boolean;  -- for left limits
		data_out : out  slv16bit_array_t(0 to KERNEL_WIDTh-1)
                                 
	);  
end component;

component conv_unit is
 Generic (   
				 constant DATA_WIDTH 		: positive := DATA_WIDTH	
				 );
				 
    Port (clk : in std_logic;
          data : in out_data (2 downto 0);
          B       : in out_data(2 downto 0);
          valid_in : in std_logic;
          Q : out  STD_LOGIC_VECTOR (2*DATA_WIDTH - 1 downto 0)
           );
end component;
component Control is
   Generic (
		constant RAM_DEPTH  : positive := IM_WIDTH;
		constant RAM_WIDTH	: positive := DATA_WIDTH
	);
	port(
        clk_i   : in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		wr_en : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (RAM_WIDTH - 1 downto 0);
		datas : out  slv16bit_array_t(0 to KERNEL_WIDTH-1);
		valid : out slv_array_t(0 to 1)
		
		);
end component;
component Flag_Generator is
 Generic (
		
		constant IM_WIDTH	: positive := IM_WIDTH
	);
 port(
        clk_i   : in  STD_LOGIC;
        valid_in : in std_logic;
		Zero_flag : out boolean;
		k1 : out boolean;
		valid_out : out std_logic;
		valid_conv: out std_logic

		);
end component;
   
signal datak :       out_data(0 to 2);
signal datas     :  slv16bit_array_t(0 to KERNEL_WIDTH-1);
signal valid     :  slv_array_t(0 to 1);
signal k1 : boolean;
signal valid_conv : std_logic;
signal Zero_flag : boolean := false;
signal Sum1 : std_logic_vector(2*DATA_WIDTH-1 downto 0);

begin
    process(clk_i)
        begin
            if (rising_edge(clk_i)) then
                Sum <= Sum1(15 downto 0);
            end if;
        end process;
 --datak1<=datak;
 MAIN_RAM : Control port map(clk_i=>clk_i,rst_i=>rst_i,wr_en=>data_valid_i,data_i=>data_i,datas=>datas,valid=>valid);
 Gen_Flags: Flag_Generator port map(clk_i=>clk_i,valid_in=>valid(0),Zero_flag=>Zero_flag,k1=>k1,valid_out=>valid_out,valid_conv=>valid_conv);
 Gen_BRAM : for i in 0 to KERNEL_WIDTH-1 generate
  Gen_first : if i =0 generate
      BRAM : BRAM1 port map(clk_i=>clk_i,rst_i=>rst_i,Zero_flag=>Zero_flag,data_valid_i=>valid(0),data_i=>datas(i),flag_left=>k1,data_out=>datak(i));
  end  generate;
  Gen_others: if i =1 generate
      BRAMk: BRAM2 port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid(0),data_i=>datas(i),flag_left=>k1,data_out=>datak(i));
  end generate;
  Gen_third: if i =2 generate
      BRAM3: BRAM2 port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid(1),data_i=>datas(i),flag_left=>k1,data_out=>datak(i));
  end generate;
 end generate;
 CONVO  : conv_unit port map(clk=>clk_i,B=>B,data=>datak,valid_in=>valid_conv,Q=>Sum1 );
           
end Behavioral;
