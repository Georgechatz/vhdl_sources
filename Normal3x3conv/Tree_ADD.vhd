library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity Tree_ADD is
     Generic (
		
		constant DATA_WIDTH	: positive 
	);
	port
	(   clk: in std_logic;
		data : in mult_data(2 downto 0);
		result : out std_logic_vector (2*DATA_WIDTH - 1 downto 0)
	);
	
end entity;

architecture rtl of Tree_ADD is

	-- Declare registers to hold intermediate sums
	signal sum1, sum2, sum3,sum4,buff1,sum5,sum6,buff2,sum7,buff3 : std_logic_vector (2*DATA_WIDTH-1 downto 0);

begin
	process(clk)
	   begin
	      if rising_edge(clk) then
		
			-- Generate and store intermediate values in the pipeline
			sum1 <= data(0)(0)+data(0)(1);
			sum2 <= data(0)(2)+data(1)(0);
			sum3 <= data(1)(1)+data(1)(2);
			sum4 <= data(2)(0)+data(2)(1);
			buff1<= data(2)(2);
			sum5 <= sum1 +sum2;
			sum6 <= sum3 +sum4;
			buff2<=buff1;
			sum7 <= sum5 +sum6;
			buff3<=buff2;
			result <= sum7 +buff3;
	    end if;
	
	   end process;
end rtl;