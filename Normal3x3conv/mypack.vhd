library ieee;
use ieee.std_logic_1164.all;

package mypack is
  type slv16bit_array_t is array (natural range <>) of std_logic_vector(15 downto 0);
  type slv32bit_array_t is array (natural range <>) of std_logic_vector(31 downto 0);
  type slv16_array_t is array (natural range <>) of std_logic_vector(17 downto 0);
  type slv8_array_t is array (natural range <>) of std_logic_vector(8 downto 0);
  type out_data is array (natural range <>) of slv16bit_array_t(2 downto 0);
  type slv_array_t is array (natural range <>) of std_logic;
  type mult_data is array (natural range <>) of slv32bit_array_t(2 downto 0);
end package;

package body mypack is
end package body;