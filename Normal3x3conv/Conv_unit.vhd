library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;

entity conv_unit is
	 Generic (   
				 constant DATA_WIDTH 		: positive ;
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 
    Port ( clk : in std_logic;
           data : in out_data (2 downto 0);
           B       : in out_data(2 downto 0);
           valid_in : in std_logic;
           Q : out  STD_LOGIC_VECTOR (2*DATA_WIDTH - 1 downto 0)
        
           );
end conv_unit;
architecture beh of conv_unit is

component Tree_ADD is
    generic( constant DATA_WIDTH :positive := DATA_WIDTH );
	port
	(   clk  : in std_logic;
	    data : in mult_data(2 downto 0);
		result : out std_logic_vector (2*DATA_WIDTH - 1 downto 0)
	);
	
end component;

   signal multi : mult_data(2 downto 0);
   
    type kernel_3x3 is array(0 to 2,0 to 2) of std_logic_vector(15 downto 0);
	--constant B : kernel_3x3 := (("1111111111110000","1111111111110000","1111111111110000"),
	--                          ("1111111111110000","0000000010000000","1111111111110000"),
	--                          ("1111111111110000","1111111111110000","1111111111110000"));
	                         --constant B : kernel_3x3 := (("1111111111110111","1111111111110001","1111111111110001"),
	                                              --       ("1111111111110100","1100101011011111","0110101101101010"),
	                                             --        ("0110101101101010","0110101101101010","0110101101101010"));
	
	begin
	  process(clk)
	    begin
	      if rising_edge(clk) then   
	       if valid_in ='1' then 
            for i in 0 to KERNEL_WIDTH-1 loop
             for j in 0 to KERNEL_WIDTH-1 loop
                multi(i)(j)<=data(KERNEL_WIDTH-1-i)(j)*B(i)(j);
             end loop;
          end loop;
          end if;
        end if;
	   end process;

	
	TREE : Tree_ADD port map(clk=>clk,data=>multi,result=>Q);
	
	
		  


end beh;