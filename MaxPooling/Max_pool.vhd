library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity Max_pool is
    Generic (
		constant IM_WIDTH  : positive:=80;
		constant DATA_WIDTH	: positive :=16
	);
	port(
        clk_i   : in  STD_LOGIC;
        rst_i   : in std_logic;
		data_valid_i : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
	    valid_out : out std_logic;
	    Maximum   : out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0)
		
		);
end entity;
architecture rtl of Max_pool is
component Control1 is
   Generic (
		constant RAM_DEPTH  : positive := IM_WIDTH;
		constant RAM_WIDTH	: positive := DATA_WIDTH
	);
	port(
        clk_i   : in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		wr_en : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (RAM_WIDTH - 1 downto 0);
		datas : out  slv16bit_array_t(0 to 1);
		valid : out std_logic
		
		);
end component;
component BRAM is
 Generic (
		
		constant DATA_WIDTH	: positive := DATA_WIDTH;
		constant IM_WIDTH   : positive := IM_WIDTH
	);
  Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		data_out : out  slv16bit_array_t(0 to 1);
        valid_out : out std_logic                        
	);  
end component;
component Comparison_Unit
  port(  clk: in std_logic;
         data_in : in out_data(0 to 1);
         valid_data : in std_logic;
         Maxim  : out std_logic_vector(15 downto 0)
        );
end component;
signal datas : slv16bit_array_t(0 to 1);
signal valid : std_logic;
signal flag1 : std_logic;
signal datak: out_data(0 to 1);
signal valid_out1 : std_logic;
constant dn1: positive := 1; 
signal delay1 : std_ulogic_vector(0 to dn1 - 1);
begin
  
  process(clk_i)
    begin
      if rising_edge(clk_i) then
       delay1<=valid_out1 & delay1(0 to dn1 - 2);
       valid_out<=delay1(dn1-1);
      end if;
    end process;
MAIN_RAM : Control1 port map(clk_i=>clk_i,rst_i=>rst_i,wr_en=>data_valid_i,data_i=>data_i,datas=>datas,valid=>valid);
BRAM_one : BRAM port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid,data_i=>datas(0),data_out=>datak(0),valid_out=>valid_out1);
BRAM_two : BRAM port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid,data_i=>datas(1),data_out=>datak(1),valid_out=>flag1);
Compare  : Comparison_Unit port map(clk=>clk_i,data_in=>datak,valid_data=>valid_out1,Maxim=>Maximum);
end rtl;