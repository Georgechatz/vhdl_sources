library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;
entity Comparison_Unit is
   port( clk : in std_logic;
         data_in : in out_data(0 to 1);
         valid_data : in std_logic;
         Maxim  : out std_logic_vector(15 downto 0)
        );
end entity;
architecture rtl of Comparison_Unit is
signal buff1,buff2: std_logic_vector(15 downto 0);
begin
   process(clk)
     begin
     if rising_edge(clk) then
     --if valid_data='1' then
       if data_in(0)(0)<=data_in(0)(1) then
         buff1<=data_in(0)(1);
       else
         buff1<=data_in(0)(0);
       end if;
    if data_in(1)(0)<=data_in(1)(1) then
         buff2<=data_in(1)(1);
       else
         buff2<=data_in(1)(0);
       end if;
        
   -- end if;
    if buff1>=buff2 then
         Maxim<=buff1;
       else
         Maxim<=buff2;
       end if; 
   end if;
   end process;
  
end rtl;