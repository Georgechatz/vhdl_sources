library ieee;
use ieee.std_logic_1164.all;

package mypack is

 type slv16bit_array_t is array (natural range <>) of std_logic_vector(15 downto 0);
 type out_data is array (natural range <>) of slv16bit_array_t(1 downto 0);
 type slv_array_t is array (natural range <>) of std_logic;
end package;

package body mypack is
end package body;