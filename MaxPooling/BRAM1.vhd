library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity BRAM is
	Generic (
		constant IM_WIDTH  : positive ;
		constant DATA_WIDTH	: positive 
	);
	Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		data_out : out  slv16bit_array_t(0 to 1);
		valid_out : out std_logic
		                       
		                                 
	);                                  
end BRAM;               
                                         

architecture rtl of BRAM is
signal cnt : integer :=0;
signal cnt2 : integer :=-1;
signal valid_out1: std_logic; 
signal valid_out2: std_logic; 
signal flag : std_logic:='0';
signal flag_end : std_logic:='1';
signal cnt_valid: integer:=0;
type fifo_t is array (0 to 1) of std_logic_vector(DATA_WIDTH-1 downto 0);
signal fifo_int : fifo_t;
--signal count   : STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0):="00000000"; 
begin    

    p0_build_5x5: process(rst_i,clk_i,flag,cnt2)
    begin
        if( rst_i = '1' )then
            fifo_int <= (others => (others => '0'));
        elsif( rising_edge(clk_i) )then
             
             if(data_valid_i = '1')then
                cnt<=cnt+1;
                cnt2<=cnt2+1; 
                if flag='0' and cnt2<IM_WIDTH-1 then
                   valid_out2<='1';
                elsif flag='0' and cnt2=IM_WIDTH-1 then
                   valid_out2<='0';
                   cnt_valid<=cnt_valid+1;
                   cnt2<=0;
                   flag<='1';
                elsif flag='1' and cnt2=IM_WIDTH-1 then
                   valid_out2<='1';
                   cnt2<=0;
                   flag<='0';
                end if;
                if cnt_valid = (IM_WIDTH)/2 then
                   flag_end <='0';
                end if;   
                
               
                if cnt =1 then
                  valid_out1<='1';
                  cnt<=0;
                else 
                  valid_out1<='0';
                end if;
                
                --count <= count + "00000001";
                
            end if;
            fifo_int(1)<=fifo_int(0);          
            fifo_int(0) <= data_i;  
        end if;
     if rising_edge(clk_i) then
        data_out(0)<=fifo_int(0);
        data_out(1)<=fifo_int(1);
        valid_out<=valid_out1 and valid_out2 and flag_end;
    end if;
 
    end process p0_build_5x5;



end rtl;