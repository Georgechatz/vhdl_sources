library ieee;
use ieee.std_logic_1164.all;

entity  Correction_Terms is
port(
      clk : in std_logic;
      sign : in std_logic_vector(3 downto 0);
      one  : in   std_logic_vector(3 downto 0);
      two  :  in std_logic_vector(3 downto 0);
      four : in std_logic;
      eight: in std_logic;
      sixteen: in std_logic;
      thirtytwo : in std_logic;
      Corr_Terms : out std_logic_vector(31 downto 0)
      );
end entity;
architecture beh of Correction_Terms is

begin
   process(clk)
      begin
      if rising_edge(clk) then
        Corr_Terms(0) <= (sign(0))and((four)or(eight)or(sixteen)or(thirtytwo));
        Corr_Terms(9 downto 1) <= "000000000";
        for i in 10 to 15 loop
           if (i)mod(2)=0 then
            Corr_Terms(i)<=(sign(i/2-4))and((one(i/2-4))or(two(i/2-4)));
           else
            Corr_Terms(i)<='0';
           end if;  
        end loop;
        Corr_Terms(23 downto 16)<="00000000";
        Corr_Terms(25 downto 24)<="11";
        for j in 26 to 31 loop
          if (j)mod(2)=0 then
             Corr_Terms(j)<='0';
          else
             Corr_Terms(j)<='1';
          end if;
        end loop;
      end if;
      end process;
 end beh;