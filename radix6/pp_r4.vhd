library ieee;
use ieee.std_logic_1164.all;
-- PARTIAL PRODUCT GENERATOR FOR R4
entity pp_r4 is
  generic(n : integer :=16);
  port(clk : in std_logic;
       a   : in std_logic_vector(n downto 0); -- a extended by one 2's complement number
       sign : in std_logic;
       one  : in std_logic;
       two  : in std_logic;
       pp_out : out std_logic_vector(n downto 0)
       );
end entity;
architecture beh of pp_r4 is
begin
  process(clk)
    begin
     if rising_edge(clk) then
      pp_out(0)<= ((a(0)xor(sign))and(one))or((sign)and(two));
      for i in 1 to n-1 loop
        pp_out(i)<=(((a(i))xor(sign))and(one))or((a(i-1)xor(sign))and(two)); 
      end loop;    
      pp_out(n)<=not(((a(n)xor(sign))and(one))or((a(n-1)xor(sign))and(two)));
     end if;
    end process;
end beh;