    library ieee;
    use ieee.std_logic_1164.all;
    -- PARTIAL PRODUCT GENERATOR FOR R4
    entity pp_r64 is
       generic(n : integer := 16);
    
       port(clk : in std_logic;
            a : in std_logic_vector(24 downto 0); -- a extended by 5 bits (2's number)
            sign : in std_logic;
            thirtytwo : in std_logic;
            sixteen  : in std_logic;
            eight    : in std_logic;
            four     : in std_logic;
            pp_out   : out std_logic_vector(24 downto 0)
            );
    end pp_r64;
    architecture beh of pp_r64 is
    signal n_ext : integer:=25;
    begin
      
      process(clk)
         begin
         if rising_edge(clk) then
           pp_out(0)<= ( (sign)and(four))or((sign)and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));   
           pp_out(1)<= ( (sign)and(four))or((sign)and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));
           pp_out(2)<= ( (sign)and(four))or((sign)and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));
           pp_out(3)<= ( (sign)and(four))or((sign)and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));
           pp_out(4)<= ( (sign)and(four))or((sign)and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));
           pp_out(5)<= ( (sign)and(four))or((sign)and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));
           pp_out(6)<= ((a(0)xor(sign))and(four))or((sign)and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));
           pp_out(7)<= ((a(1)xor(sign))and(four))or((a(0)xor(sign))and(eight))or((sign)and(sixteen))or((sign)and(thirtytwo));
           pp_out(8)<= ((a(2)xor(sign))and(four))or((a(1)xor(sign))and(eight))or((a(0)xor(sign))and(sixteen))or((sign)and(thirtytwo));
           for i in 9 to 23 loop
              pp_out(i)<=((a(i-6)xor(sign))and(four))or((a(i-7)xor(sign))and(eight))or((a(i-8)xor(sign))and(sixteen))or((a(i-9)xor(sign))and(thirtytwo));
           end loop;
           pp_out(n_ext-1)<= not(
                            ((a(n_ext-7)xor(sign))and(four))or((a(n_ext-8)xor(sign))and(eight))or((a(n_ext-9)xor(sign))and(sixteen))or((a(n_ext-10)xor(sign))and(thirtytwo)) 
                           );
         end if;
         end process;
    end beh;