library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library work;
use work.mypack.all;
entity Adding_Unit is
port  (  clk : in std_logic;
         pp_r64_out : in std_logic_vector(24 downto 0);
         pp_r4_out  : in slv17bit_array_t(2 downto 0);
         Corr_Terms : in std_logic_vector(31 downto 0);
         result     : out std_logic_vector(31 downto 0)
         );
end entity;
architecture rtl of Adding_Unit is
signal PP : slv32bit_array_t(4 downto 0);
signal sums1 : slv32bit_array_t(2 downto 0);
signal sums2 : slv32bit_array_t(1 downto 0);
begin 
  process(clk)
   begin
    if rising_edge(clk) then
  
  
     PP(0)<="0000000"&pp_r64_out;     
     PP(1)<="00000"&(pp_r4_out(0))&"0000000000";
     PP(2)<="000"&(pp_r4_out(1))&"000000000000";
     PP(3)<="0"&(pp_r4_out(2))&"00000000000000";
     PP(4)<=Corr_Terms;
     --
     sums1(0)<=PP(0)+PP(1);
     sums1(1)<=PP(2)+PP(3);
     sums1(2)<=PP(4);
     --sums1(3)<=PP(6);
     --
     sums2(0)<=sums1(0)+sums1(1);
     sums2(1)<=sums1(2);
     --
     result<=sums2(0)+sums2(1);
    end if;
   end process; 

end rtl;