library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity conv_unit is
	 Generic (   
				 constant DATA_WIDTH 		: positive ;
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 
    Port ( clk : in std_logic;
           data : in out_data (KERNEL_WIDTH downto 0);
           Q : out  STD_LOGIC_VECTOR (2*DATA_WIDTH - 1 downto 0)
           );
end conv_unit;
