library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;
entity Kernel_Transform_Unit is
    port( clk: in std_logic;
          In_Kernel : in kern_data(0 to 2);
          Out_Kernel : out tr_kern_data(0 to 3)
          );
 end entity;
 architecture rtl of Kernel_Transform_Unit is
 signal s : slv8bit_array_t(0 to 15);
 signal Mult_half : std_logic_vector(7 downto 0):= "00001000"; --1/2
 signal Mult_quad : std_logic_vector(7 downto 0):= "00000100"; --1/4
 signal Mult_One  : std_logic_vector(7 downto 0):= "00010000"; --1
 begin
   process(clk)
     begin
      if rising_edge(clk) then
        -- first line
        s(0)<=In_Kernel(0)(0);
        s(1)<=In_Kernel(0)(0)+In_Kernel(0)(1)+In_Kernel(0)(2);
        s(2)<=In_Kernel(0)(0)+In_Kernel(0)(2)-In_Kernel(0)(1);
        s(3)<=In_Kernel(0)(2);
        -- second_line
        s(4)<=In_Kernel(0)(0)+In_Kernel(1)(0)+In_Kernel(2)(0);
        s(5)<=In_Kernel(0)(0)+In_Kernel(2)(0)+In_Kernel(1)(0)+In_Kernel(0)(1)+In_Kernel(1)(1)+In_Kernel(2)(1)+In_Kernel(0)(2)+In_Kernel(1)(2)+In_Kernel(2)(2);
        s(6)<=In_Kernel(0)(0)+In_Kernel(2)(0)+In_Kernel(1)(0)+In_Kernel(0)(2)+In_Kernel(1)(2)+In_Kernel(2)(2)-In_Kernel(0)(1)-In_Kernel(1)(1)-In_Kernel(2)(1);
        s(7)<=In_Kernel(0)(2)+In_Kernel(2)(2)+In_Kernel(1)(2);
        -- thrid line
        s(8)<=In_Kernel(0)(0)+In_Kernel(2)(0)-In_Kernel(1)(0);
        s(9)<=In_Kernel(0)(0)+In_Kernel(2)(0)+In_Kernel(0)(1)+In_Kernel(2)(1)+In_Kernel(0)(2)+In_Kernel(2)(2)-In_Kernel(1)(0)-In_Kernel(1)(2)-In_Kernel(1)(1);
        s(10)<=In_Kernel(0)(0)+In_Kernel(2)(0)+In_Kernel(1)(1)+In_Kernel(0)(2)+In_Kernel(2)(2)-In_Kernel(1)(0)-In_Kernel(0)(1)-In_Kernel(2)(1)-In_Kernel(1)(2);
        s(11)<=In_Kernel(0)(2)+In_Kernel(2)(2)-In_Kernel(1)(2);
        -- fourth line
        s(12)<=In_Kernel(2)(0);
        s(13)<=In_Kernel(2)(0)+In_Kernel(2)(1)+In_Kernel(2)(2);
        s(14)<=In_Kernel(2)(0)+In_Kernel(2)(2)-In_Kernel(2)(1);
        s(15)<=In_kernel(2)(2);
        -- Multiplications
        Out_Kernel(0)(0)<=s(0)*Mult_One;
        Out_Kernel(0)(1)<=s(1)*Mult_half;
        Out_Kernel(0)(2)<=s(2)*Mult_half;
        Out_Kernel(0)(3)<=s(3)*Mult_One;
        Out_Kernel(1)(0)<=s(4)*Mult_half;
        Out_Kernel(1)(1)<=s(5)*Mult_quad;
        Out_Kernel(1)(2)<=s(6)*Mult_quad;
        Out_Kernel(1)(3)<=s(7)*Mult_half;
        Out_Kernel(2)(0)<=s(8)*Mult_half;
        Out_Kernel(2)(1)<=s(9)*Mult_quad;
        Out_Kernel(2)(2)<=s(10)*Mult_quad;
        Out_Kernel(2)(3)<=s(11)*Mult_half;
        Out_Kernel(3)(0)<=s(12)*Mult_One;
        Out_Kernel(3)(1)<=s(13)*Mult_half;
        Out_Kernel(3)(2)<=s(14)*Mult_half;
        Out_Kernel(3)(3)<=s(15)*Mult_One;
      end if;
    end process;
 
 end rtl;