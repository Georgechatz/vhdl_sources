library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity Input_Transform_Unit is
    Generic (   
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 
    port( clk: in std_logic;
          in_data : in out_data(0 to KERNEL_WIDTH);
          Outp    : out out_data(0 to KERNEL_WIDTh)
         
          );
 end entity;
 architecture rtl of Input_Transform_Unit is
 signal z,x,y,n : slv16bit_array_t(0 to KERNEL_WIDTH);
 signal valid_k,valid_k2 :std_logic;
 begin
  process(clk)
    begin
      if rising_edge(clk) then
      --if (valid_data='1') then 
       
           -- first line
           z(0) <=in_data(3)(0)-in_data(1)(0);
           z(1) <=in_data(3)(2)-in_data(1)(2);
           z(2) <=in_data(3)(1)-in_data(1)(1);
           z(3) <=in_data(1)(3)-in_data(3)(3);
           
           -- second line
           x(0)<=in_data(2)(0)+in_data(1)(0);
           x(1)<=in_data(2)(2)+in_data(1)(2);
           x(2)<=in_data(2)(1)+in_data(1)(1);
           x(3)<=in_data(2)(3)+in_data(1)(3);
           
           -- third line
           y(0)<=in_data(1)(0)-in_data(2)(0);
           y(1)<=in_data(1)(2)-in_data(2)(2);
           y(2)<=in_data(1)(1)-in_data(2)(1);
           y(3)<=in_data(2)(3)-in_data(1)(3);
           
           --fourth line
           n(0)<=in_data(2)(0)-in_data(0)(0);
           n(1)<=in_data(2)(2)-in_data(0)(2);
           n(2)<=in_data(2)(1)-in_data(0)(1);
           n(3)<=in_data(0)(3)-in_data(2)(3);
           
           Outp(0)(0)<=z(0)-z(1);
           Outp(0)(1)<=z(2)+z(1);
           Outp(0)(2)<=z(1)-z(2);
           Outp(0)(3)<=z(2)+z(3);
           --
           Outp(1)(0)<=x(0)-x(1);
           Outp(1)(1)<=x(1)+x(2);
           Outp(1)(2)<=x(1)-x(2);
           Outp(1)(3)<=x(2)-x(3);
           --
           Outp(2)(0)<=y(0)-y(1);
           Outp(2)(1)<=y(2)+y(1);
           Outp(2)(2)<=y(1)-y(2);
           Outp(2)(3)<=y(2)+y(3);
           --
           Outp(3)(0)<=n(0)-n(1);
           Outp(3)(1)<=n(2)+n(1);
           Outp(3)(2)<=n(1)-n(2);
           Outp(3)(3)<=n(2)+n(3);
       
       --end if;
      end if;
     end process;
   
 end rtl;