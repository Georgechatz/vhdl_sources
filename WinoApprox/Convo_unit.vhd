library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity convo_unit is
    Generic (   
				 constant DATA_WIDTH 		: positive ;
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 
    port( clk : in std_logic;
          c : in kern_data(0 to 2);
          in_data : in out_data(0 to KERNEL_WIDTH);
          Sum     : out slv32bit_array_t(0 to KERNEL_WIDTH)
          
         
          );
 end entity;
 architecture rtl of convo_unit is
 --signal c : kern_data(0 to 2);

 component Input_Transform_Unit is
     generic( constant KERNEL_WIDTH : positive :=3
        );
     port(clk : in std_logic;
          in_data : in out_data(0 to KERNEL_WIDTH);
          Outp    : out out_data(0 to KERNEL_WIDTh)
          
          
          );
 end component;
 component Kernel_Transform_Unit is
       port( clk : in std_logic;
             In_Kernel : in kern_data(0 to 2);
             Out_Kernel : out tr_kern_data(0 to 3)
          );
 end component;
 component Multiplication_Unit is
      Generic (   
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 
   port (clk : in std_logic; 
         Trans_Data: in out_data(0 to KERNEL_WIDTH);
         Trans_kernel : in tr_kern_data(0 to KERNEL_WIDTH);
         Mult_out : out mult_data(0 to KERNEL_WIDTH)
        
         
         );
 end component; 
 component Add_Unit is 
     Generic (   
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 

   port(clk : in std_logic;
        Inp : in mult_data(0 to KERNEL_WIDTH);
        Sum : out slv32bit_array_t(0 to KERNEL_WIDTH)
        
        );
 end component;
 signal valid_trans : std_logic;
 signal valid_mult  : std_logic;
 signal Outp : out_data(0 to KERNEL_WIDTh);
 signal Tr_Kernel : tr_kern_data(0 to KERNEL_WIDTH);
 signal multi_data : mult_data(0 to KERNEL_WIDTH);
    begin
     --  process(clk)
       --   begin
        --     if rising_edge(clk) then
            
        --    end if;
       -- end process;
    Input_Transform:  Input_Transform_Unit port map(clk=>clk,in_data=>in_data,Outp=>Outp);
    Kernel_Transform: Kernel_Transform_Unit port map(clk=>clk,In_Kernel=>C,Out_Kernel=>Tr_Kernel);
    Multi_Unit : Multiplication_Unit port map(clk=>clk,Trans_Data=>Outp,Trans_kernel=>Tr_Kernel,Mult_out=>multi_data);
    ADD_TREE   : Add_unit port map(clk=>clk,Inp=>multi_data,Sum=>Sum);
 end rtl;