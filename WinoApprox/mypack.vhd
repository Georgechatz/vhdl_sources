library ieee;
use ieee.std_logic_1164.all;

package mypack is
  type slv8bit_array_t is array (natural range <>) of std_logic_vector(7 downto 0);
  type slv27bit_array_t is array (natural range <>) of std_logic_vector(26 downto 0);
  type slv32bit_array_t is array (natural range <>) of std_logic_vector(31 downto 0);
  type slv17bit_array_t is array (natural range <>) of std_logic_vector(16 downto 0);
  type slv16bit_array_t is array (natural range <>) of std_logic_vector(15 downto 0);
  type Tr_kern_data is array (natural range <>) of slv16bit_array_t(3 downto 0);
  type kern_data is array (natural range <>) of slv8bit_array_t(2 downto 0);
  type slv8_array_t is array (natural range <>) of std_logic_vector(10 downto 0);
  type out_data is array (natural range <>) of slv16bit_array_t(3 downto 0);
  type mult_data is array (natural range <>) of slv32bit_array_t(3 downto 0);
  type slv_array_t is array (natural range <>) of std_logic;

end package;

package body mypack is
end package body;