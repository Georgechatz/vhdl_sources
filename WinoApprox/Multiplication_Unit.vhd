library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;
entity Multiplication_Unit is
  Generic (   
				 constant KERNEL_WIDTH      : positive := 3
				 );		 
   port (clk : in std_logic;
         Trans_Data: in out_data(0 to KERNEL_WIDTH);
         Trans_kernel : in tr_kern_data(0 to KERNEL_WIDTH);
         Mult_out : out mult_data(0 to KERNEL_WIDTH)
     
         );
 end entity;

architecture rtl of Multiplication_Unit is
 component rad_64 is
  generic(n : integer :=16);
  port(clk : in std_logic;
       a   : in std_logic_vector(n-1 downto 0);
       b   : in std_logic_vector(n-1 downto 0);
       result : out std_logic_vector(2*n-1 downto 0)
 
     );
end component;

begin
    Gen_Mul : for i in 0 to KERNEL_WIDTH generate
	  Gen_Mul2 : for j in 0 to KERNEL_WIDTH generate
	      MULT : rad_64 port map(clk=>clk,a=>Trans_kernel(i)(j),b=>Trans_Data(i)(j),result=> Mult_out(i)(j));
	  end generate;
	end generate;
end rtl;