library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;
entity Multiplication_Unit is
  Generic (   
				 constant KERNEL_WIDTH      : positive := 3
				 );		 
   port (clk : in std_logic;
         Trans_Data: in out_data(0 to KERNEL_WIDTH);
         Trans_kernel : in tr_kern_data(0 to KERNEL_WIDTH);
         Mult_out : out mult_data(0 to KERNEL_WIDTH)
     
         );
 end entity;
architecture rtl of Multiplication_Unit is


begin
   process(clk)
      begin
       if rising_edge(clk) then
        --if (valid_in ='1') then
         for i in  0 to KERNEL_WIDTH loop
             for j in  0 to KERNEL_WIDTH loop
                 Mult_out(i)(j)<=Trans_Data(i)(j)*Trans_kernel(i)(j);
             end loop;
          end loop;
        -- end if;
       end if;
       end process;
end rtl;