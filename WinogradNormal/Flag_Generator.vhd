library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity Flag_Generator is
    Generic (
		constant IM_WIDTH  : positive

	);
	port(
        clk_i   : in  STD_LOGIC;
        valid_flag3 : in std_logic;
		Zero_flag : out boolean;
		flag_ari : out boolean;
		valid_out_data : out std_logic;
		valid_out : out std_logic

		);
end Flag_Generator;


architecture beh of Flag_Generator is
signal count1: integer:=0;
signal count2: integer:=0;
signal count3: integer:=0;
signal count4: integer:=0;
signal count5: integer:=0;
signal count6: integer:=0;
signal flag5 : boolean;
signal flag6 : boolean:=false;
signal Zero_flag1 : boolean:=false;
signal flag4 : boolean:=false;
constant dn: positive := 2;
signal valid_out1: std_logic;
signal delay: std_ulogic_vector(0 to dn - 1);
signal valid_flag2 : std_logic;
--signal flag_ari : boolean;
constant dn2: positive := 6;
signal delay2: std_ulogic_vector(0 to dn2 - 1);
constant dn1: positive := 8;
signal delay1: std_ulogic_vector(0 to dn1 - 1);
signal valid_out_data1 : std_logic;
begin
proc_control : process(clk_i,count3)
  begin
  if rising_edge(clk_i) then
      delay1<=valid_flag3&delay1(0 to dn1-2);
      delay2<=valid_flag3&delay2(0 to dn2-2);
      valid_out_data1<=delay2(dn2-1);
      valid_flag2<=delay1(dn1-1); 
      count4<=count4+1;
      if count4= (3)*(IM_WIDTH)+11 then
         valid_out1<='1';
         flag5<=true;
      end if;
      if flag5 then
         count5<=count5+1;
         if count5 = IM_WIDTH then
            count6<=count6+1;
            count5<=0;
         end if;
      end if;
      if count6 = IM_WIDTH-2 then
         valid_out1<='0';
      end if;
      if count6 = IM_WIDTH-4 then
         Zero_flag<=true;
      end if;
      if count3 = 2*IM_WIDTH+3 then
          flag4<=true;
      end if;
       --valid_flag<=(valid_flag3)or(valid_flag2); 
  end if;
  
  if falling_edge(clk_i) then
    count3<=count3+1;
    if flag4 then
    count1<=count1+1;
    end if;
   if count1 = IM_WIDTH-2 and flag4 then
       flag_ari<= true;
    elsif count1 = IM_WIDTH-1 and flag4 then
       flag_ari<=false;
       count1<=0;
    else
      flag_ari<=false;
    end if;
   end if;
 end process;
 valid_out<=valid_out1 and valid_out_data1;
 end beh;