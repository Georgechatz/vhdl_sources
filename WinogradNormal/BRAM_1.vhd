library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;use 
IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
library work;
use work.mypack.all;
entity BRAM1 is
	Generic (
		
		constant DATA_WIDTH	: positive ;
		constant KERNEL_SIZE : positive:=3
	);
	Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		flag_left : in boolean; 
		data_out : out  slv16bit_array_t(0 to KERNEL_SIZE);
		kalo_flag : out std_logic
		                        
		                                 
	);                                  
end BRAM1;               
                                         

architecture rtl of BRAM1 is
--type fifo_t is array (0 to 2*IM_WIDTH + 2) of std_logic_vector(7 downto 0);
type bool is array (0 to 4 ) of boolean;
signal flags : bool := (false,false,false,false,false);
type fifo_t is array (0 to KERNEL_SIZE) of std_logic_vector(DATA_WIDTH-1 downto 0);
signal kalo_flag1 : std_logic;
signal kalo_flag2 : std_logic;
signal kalo_flag3 : std_logic;
signal fifo_int : fifo_t;
signal flag1: boolean := true;
signal cnt : integer :=0;
signal cnt2 : integer :=0;
signal load_flag : std_logic;
signal load_flag1 : std_logic;
signal load_flag2 : std_logic;
--signal load_flag : std_logic :='1';
--signal count   : STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0):="00000000";
signal count : integer :=0; 
begin    

    p0_build_5x5: process(rst_i,clk_i,flags(0),flags(1),flags(2),flags(3),flags(4))
    begin
         if( rst_i = '1' )then
            fifo_int <= (others => (others => '0'));
        elsif( rising_edge(clk_i) )then
             cnt<=cnt+1;
             if cnt =1 then
               kalo_flag1<='1';
               cnt<=0;
             else 
               kalo_flag1<='0';
              
             end if;
             if(data_valid_i = '1')then
                count<=count+1;
               
                if flag_left then
                   cnt2<=cnt2+1;
                   if cnt2=1 then
                      load_flag1 <='0';
                      cnt2<=0;
                   else 
                      load_flag1 <='1';
                   end if;
                end if;
                   
                for i in 1 to KERNEL_SIZE loop
                    fifo_int(i) <= fifo_int(i-1);
                end loop;           
                fifo_int(0) <= data_i;  
             end if;
        end if;
     if rising_edge(clk_i) then
          load_flag2<=load_flag1;
          load_flag <=load_flag2;
          kalo_flag<=kalo_flag1 and load_flag2;
          if flag_left or flags(0) or flags(1) then
          if flag_left then 
             for i in 1 to KERNEL_SIZE loop
                 data_out(i)<=fifo_int(i);
             end loop;
             data_out(0)<= (others=>'0');
             flags(0)<=true;
          elsif flags(0) then
             for i in 0 to KERNEL_SIZE loop
                data_out(i)<=fifo_int(i);
             end loop;
             flags(1)<=true;
             flags(0)<=false;
          elsif flags(1) then
             for i in 0 to KERNEL_SIZE-1 loop
                data_out(i)<=fifo_int(i);
             end loop;
             data_out(3)<=(others=>'0');
             flags(0)<=false;
             flags(1)<=false;
          end if;
          else
          for i in 0 to KERNEL_SIZE loop
             data_out(i) <= fifo_int(i);
          end loop;  
          end if;
      
     end if;
    end process p0_build_5x5;
   


end rtl;