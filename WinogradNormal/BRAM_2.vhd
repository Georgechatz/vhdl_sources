library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;use 
IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
library work;
use work.mypack.all;
entity BRAM2 is
	Generic (
		--constant IM_WIDTH  : positive := 80;
		constant DATA_WIDTH	: positive ;
		constant KERNEL_SIZE : positive:=3
	);
	Port ( 
		clk_i		: in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		data_valid_i : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		flag_left : in boolean;  -- for left limits
		data_out : out  slv16bit_array_t(0 to KERNEL_SIZE);
		valids1 : out std_logic
	                                 
	);                                  
end BRAM2;               
                                         

architecture rtl of BRAM2 is
--type fifo_t is array (0 to 2*IM_WIDTH + 2) of std_logic_vector(7 downto 0);
signal kati : boolean :=false;
type bool is array (0 to 4 ) of boolean;
signal flags : bool := (false,false,false,false,false);
signal flags1 : bool :=(false,false,false,false,false);
type fifo_t is array (0 to KERNEL_SIZE) of std_logic_vector(DATA_WIDTH-1 downto 0);
signal fifo_int : fifo_t;
signal valids : std_logic;
constant dn: positive := 2;
signal delay1 : std_ulogic_vector(0 to dn - 1);
begin    

    p0_build_5x5: process(rst_i,clk_i,flags,flag_left,valids,kati)
    begin
        if( rst_i = '1' )then
            fifo_int <= (others => (others => '0'));
        elsif( rising_edge(clk_i) )then
             if(data_valid_i = '1')then
                for i in 1 to KERNEL_SIZE loop
                    fifo_int(i) <= fifo_int(i-1);
                end loop;           
                fifo_int(0) <= data_i;  
            else
               fifo_int <= (others => (others => '0'));
            end if;
            
        end if;
        
        
    if rising_edge(clk_i) then
       delay1 <= data_valid_i & delay1(0 to dn - 2);
       valids <=delay1(dn-1);
       if valids ='0' then
          for i in 0 to KERNEL_SIZE loop
              data_out(i)<= (others=>'0');
          end loop;
       else
       if flag_left or flags(0) or flags(1) then
          if flag_left then 
             for i in 1 to KERNEL_SIZE loop
                 data_out(i)<=fifo_int(i);
             end loop;
             data_out(0)<= (others=>'0');
             flags(0)<=true;
          elsif flags(0) then
             for i in 0 to KERNEL_SIZE loop
                data_out(i)<=fifo_int(i);
             end loop;
             flags(1)<=true;
             flags(0)<=false;
          elsif flags(1) then
             for i in 0 to KERNEL_SIZE-1 loop
                data_out(i)<=fifo_int(i);
             end loop;
             data_out(3)<=(others=>'0');
             flags(0)<=false;
             flags(1)<=false;
          end if;
          else
          for i in 0 to KERNEL_SIZE loop
             data_out(i) <= fifo_int(i);
          end loop;  
          end if;
       end if;
       end if;
 end process p0_build_5x5;
   


end rtl;