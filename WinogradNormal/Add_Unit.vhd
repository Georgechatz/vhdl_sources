library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;
entity Add_Unit is
  Generic (   
				 constant KERNEL_WIDTH      : positive 
				 );
				 

   port(clk : in std_logic;
        Inp : in mult_data(0 to KERNEL_WIDTH);
        Sum : out slv32bit_array_t(0 to KERNEL_WIDTH)
        
        );
end entity;
architecture rtl of Add_Unit is
signal sums : slv32bit_array_t(15 downto 0);
signal sums1 : slv32bit_array_t(11 downto 0);
signal sums2 : slv32bit_array_t(7 downto 0);
signal valid1,valid2,valid3,valid4,valid5,valid6 : std_logic;

begin
   process(clk)
    begin
      if rising_edge(clk) then
         
       
             sums(0)<=Inp(0)(1)+Inp(1)(1);
             sums(1)<=Inp(0)(2)+Inp(1)(2);
             sums(2)<=Inp(0)(0)+Inp(1)(0);
             sums(3)<=Inp(2)(1); -- Y10
             sums(4)<=Inp(2)(2); --Y11
             sums(5)<=Inp(2)(0); --Y9
             --
             sums(6)<=Inp(1)(3)+Inp(2)(3);
             sums(7)<=Inp(0)(3);
             --
             sums(8)<=Inp(1)(1)-Inp(2)(1);
             sums(9)<=Inp(1)(2)-Inp(2)(2);
             sums(10)<=Inp(3)(1);
             sums(11)<=Inp(3)(2);
             sums(12)<=Inp(3)(0);--Y13
             sums(13)<=Inp(1)(0)-Inp(2)(0);
             sums(14)<=Inp(3)(3);
             sums(15)<=Inp(2)(3)-Inp(1)(3);
             --
             
             
             
             
             sums1(0)<=sums(2);
             sums1(1)<=sums(0)+sums(3);--1
             sums1(2)<=sums(1)+sums(4);--2
             sums1(3)<=sums(5);
             --
             sums1(4)<=sums(6);
             sums1(5)<=sums(7);
             --
             sums1(6)<=sums(8)-sums(10); --3
             sums1(7)<=sums(9)-sums(11); --4
             sums1(8)<=sums(12);
             sums1(9)<=sums(13);
             --
             sums1(10)<=sums(14);
             sums1(11)<=sums(15);
             --
             
             sums2(0)<=sums1(0)+sums1(1);
             sums2(1)<=sums1(2)+sums1(3);
             --
             sums2(2)<=sums1(1)-sums1(5); --1 -Y4
             sums2(3)<=sums1(2)+sums1(4); --2+Y8+Y12
             -- z2 = sums2(2)-sums2(3)
             --
             sums2(4)<=sums1(9)-sums1(8);
             sums2(5)<=sums1(6)+sums1(7);
             --
             sums2(6)<=sums1(10)+sums1(11);
             sums2(7)<=sums1(6)-sums1(7);
             
             --
             Sum(0)<=sums2(0)+sums2(1);
             Sum(1)<=sums2(2)-sums2(3);
             Sum(2)<=sums2(4)+sums2(5);
             Sum(3)<=sums2(6)+sums2(7);
 
            end if;
       end process;
end rtl;