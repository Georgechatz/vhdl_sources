library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Package gia na dinw input output pinakes 
library work;
use work.mypack.all;
entity Control is
    Generic (
		constant RAM_DEPTH  : positive ;
		constant RAM_WIDTH	: positive ;
		constant KERNEL_WIDTH: positive :=3
	);
	port(
        clk_i   : in  STD_LOGIC;
		rst_i	: in  STD_LOGIC;
		wr_en : in std_logic;
		data_i	: in  STD_LOGIC_VECTOR (RAM_WIDTH - 1 downto 0);
		datas : out  slv16bit_array_t(0 to KERNEL_WIDTH);
		valid : out slv_array_t(0 to KERNEL_WIDTH-2)
	
		);
end Control;
architecture Behavioral of Control is
  component STD_FIFO is 
     Generic (
		constant DATA_WIDTH  : positive := RAM_WIDTH;
		constant FIFO_DEPTH	: positive := RAM_DEPTH
	);
	Port ( 
		CLK		: in  STD_LOGIC;
		RST		: in  STD_LOGIC;
		WriteEn	: in  STD_LOGIC;
		DataIn	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		ReadEn	: in  STD_LOGIC;
		DataOut	: out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		Empty	: out STD_LOGIC;
		Full	: out STD_LOGIC
	);
   end component;
   component ring_buffer2 is 
      generic (
    RAM_WIDTH : natural:=RAM_WIDTH;
    RAM_DEPTH : natural:=RAM_DEPTH
  );
  port (
    clk : in std_logic;
    rst : in std_logic;
    wr_en : in std_logic;
    wr_data : in std_logic_vector(RAM_WIDTH - 1 downto 0);
    rd_en : in std_logic;
    rd_valid : out std_logic;
    rd_data : out std_logic_vector(RAM_WIDTH - 1 downto 0);
    empty : out std_logic;
    empty_next : out std_logic;
    full : out std_logic;
    full_next : out std_logic;
    fill_count : out integer range RAM_DEPTH - 1 downto 0
    );
   end component;
  component ring_buffer3 is 
      generic (
    RAM_WIDTH : natural:=RAM_WIDTH;
    RAM_DEPTH : natural:=RAM_DEPTH
  );
  port (
    clk : in std_logic;
    rst : in std_logic;
    wr_en : in std_logic;
    wr_data : in std_logic_vector(RAM_WIDTH - 1 downto 0);
    rd_en : in std_logic;
    rd_valid : out std_logic;
    rd_data : out std_logic_vector(RAM_WIDTH - 1 downto 0);
    empty : out std_logic;
    empty_next : out std_logic;
    full : out std_logic;
    full_next : out std_logic;
    fill_count : out integer range RAM_DEPTH - 1 downto 0
    );
   end component;

constant dn1: positive := 5; 
constant dn2: positive := 7; 
constant dn3: positive := 9; 
constant dn: positive := 3; 
--type delay1 is array (0 to 4) of  std_ulogic_vector(0 to dn - 1); 
signal delay1 : std_ulogic_vector(0 to dn - 1);
signal delay2 : std_ulogic_vector(0 to dn1 - 1);
signal delay3 : std_ulogic_vector(0 to dn2 - 1);
signal delay4 : std_ulogic_vector(0 to dn3 - 1);
--signal delay1 : std_ulogic_vector(0 to RAM_WIDTH+dn1-1 );
type data_type is array (0 to 15) of STD_LOGIC_VECTOR (RAM_WIDTH - 1 downto 0); 
signal data : data_type;
type out_type is array (0 to 8) of STD_LOGIC_VECTOR (RAM_WIDTH - 1 downto 0); 
signal outp : out_type;
type empty_type is array (0 to 8) of STD_LOGIC; 
signal empty : empty_type;
type full_type is array (0 to 8) of STD_LOGIC; 
signal full : full_type;
type empty_next_type is array (0 to 8) of STD_LOGIC; 
signal empty_next : empty_next_type;
type full_next_type is array (0 to 8) of STD_LOGIC; 
signal full_next : full_next_type;
type fill_count_type is array (0 to 8) of integer range RAM_DEPTH - 1 downto 0; 
signal fill_count : fill_count_type;
type rd_en_type is array (0 to 8) of std_logic; 
signal rd_en : rd_en_type;
type rd_valid_type is array (0 to 8) of std_logic; 
signal wr_enabl : rd_en_type;
signal rd_enabl : rd_en_type;
signal rd_valid : rd_valid_type;

signal rd_en1 : std_logic;
type bool is array (0 to 4 ) of std_logic;
signal flags : bool := ('0','0','0','0','0');
signal count: integer:=0;

 begin
 

 proc_control : process(clk_i)
  begin
  if rising_edge(clk_i) then
      count<=count+1;
      if count>RAM_DEPTH and flags(0)='0' then
         flags(0)<='1';
      end if;
      if count>2*RAM_DEPTH and flags(1)='0' then
         flags(1)<='1';
      end if;
      if count>3*RAM_DEPTH and flags(2)='0' then
         flags(2)<='1';
      end if;
      if count>4*RAM_DEPTH and flags(3)='0' then
         flags(3)<='1';
      end if;
  end if;
  if full(0) ='1' and flags(0)='0' then
      rd_en1<='1';
  elsif flags(0)='1' then
      rd_en1<='1';
  end if;
  for j in  0 to 3 loop
      rd_en(j)<=full_next(j);
  end loop;

    -- PIPELINE DELAYS
  if rising_edge(clk_i) then
     
     datas(1)<=outp(1);
     --
     data(0)<=outp(2);
     datas(2)<=data(0);
     --
     data(1)<=outp(3);
     data(2)<=data(1);
     datas(3)<=data(2);

   end if;
   

    if rising_edge(clk_i) then
         delay2 <= rd_valid(2) & delay2(0 to dn1 - 2);
         delay1 <= rd_valid(1) & delay1(0 to dn  - 2);
    end if;
    if rising_edge(clk_i) then
        valid(0)<=delay1(dn-1);
        valid(1)<=delay2(dn1-1);
   
    end if;

  end process;
    wr_enabl(0)<=full(0) or flags(1);
    rd_enabl(0)<=rd_en(0) or flags(1);
    wr_enabl(1)<=full_next(0) or flags(2);
    rd_enabl(1)<=rd_en(1) or flags(2);
    wr_enabl(2)<=full_next(1) or flags(3);
    rd_enabl(2)<=rd_en(2) or flags(3);
    FIFO_1: STD_FIFO port map(clk=>clk_i,rst=>rst_i,WriteEn =>wr_en, DataIn =>data_i, ReadEn =>rd_en1,DataOut=>outp(0), Empty =>empty(0), Full=>full(0) );
Gen_FIFO: for i in 0 to KERNEL_WIDTH-1 generate
    Gen_FIFO1: if i=0 generate
    FIFO_21: ring_buffer2 port map(
    clk=>clk_i,rst=>rst_i, wr_en =>wr_enabl(i),wr_data =>outp(i),rd_en =>rd_enabl(i),rd_valid =>rd_valid(i),rd_data=>outp(i+1),empty =>empty(i+1), empty_next=>empty_next(i),full=>full(i+1),full_next=>full_next(i),fill_count=>fill_count(i));
    end generate;
    Gen_FIFO12 : if i /= 0 generate
    FIFO_21: ring_buffer2 port map(
    clk=>clk_i,rst=>rst_i, wr_en =>wr_enabl(i),wr_data =>outp(i),rd_en =>rd_enabl(i),rd_valid =>rd_valid(i),rd_data=>outp(i+1),empty =>empty(i+1), empty_next=>empty_next(i),full=>full(i+1),full_next=>full_next(i),fill_count=>fill_count(i));
    end generate;
 end generate;
    datas(0)<=outp(0);
  
 end Behavioral;