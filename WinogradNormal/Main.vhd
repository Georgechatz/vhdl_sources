library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unSIGNED.ALL;
library work;
use work.mypack.all;

entity Main is
Generic (
    constant IM_WIDTH  : positive:=80;
    constant DATA_WIDTH	: positive :=16;
    constant KERNEL_WIDTH :positive :=3
);
port(
    clk_i   : in  STD_LOGIC;
    rst_i	: in  STD_LOGIC;
    c : in kern_data(0 to 2);
    data_valid_i : in std_logic;
    data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
    Sum     : out slv32bit_array_t(0 to KERNEL_WIDTH);
    valid_out : out std_logic
    
    

    );
end Main;
architecture Behavioral of Main is

component Control is
Generic (
    constant RAM_DEPTH  : positive := IM_WIDTH;
    constant RAM_WIDTH	: positive := DATA_WIDTH
);
port(
    clk_i   : in  STD_LOGIC;
    rst_i	: in  STD_LOGIC;
    wr_en : in std_logic;
    data_i	: in  STD_LOGIC_VECTOR (RAM_WIDTH - 1 downto 0);
    datas : out  slv16bit_array_t(0 to KERNEL_WIDTH);
    valid : out slv_array_t(0 to KERNEL_WIDTH-2)
    
    );
end component;
component BRAM_one is
Generic (
    
    constant DATA_WIDTH	: positive := DATA_WIDTH
);
Port ( 
    clk_i		: in  STD_LOGIC;
    rst_i	: in  STD_LOGIC;
    data_valid_i : in std_logic;
    data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
    flag_left : in boolean; 
    Zero_flag : in boolean;
    data_out : out  slv16bit_array_t(0 to KERNEL_WIDTh);
    kalo_flag : out std_logic
    
               
);  
end component;
component BRAM1 is
Generic (
    
    constant DATA_WIDTH	: positive := DATA_WIDTH
);
Port ( 
    clk_i		: in  STD_LOGIC;
    rst_i	: in  STD_LOGIC;
    data_valid_i : in std_logic;
    data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
    flag_left : in boolean; 
    data_out : out  slv16bit_array_t(0 to KERNEL_WIDTh);
    kalo_flag : out std_logic
    
               
);  
end component;
component BRAM2 is
Generic (
    
    constant DATA_WIDTH	: positive := DATA_WIDTH
);
Port ( 
    clk_i		: in  STD_LOGIC;
    rst_i	: in  STD_LOGIC;
    data_valid_i : in std_logic;
    data_i	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
    flag_left : in boolean;  -- for left limits
    data_out : out  slv16bit_array_t(0 to KERNEL_WIDTh)
                             
);  
end component;
component convo_unit is
Generic (   
             constant DATA_WIDTH 		: positive := DATA_WIDTH;
             constant KERNEL_WIDTH      : positive := KERNEL_WIDTH
             );
port( clk: in std_logic;
      c : in kern_data(0 to 2);
      in_data : in out_data(0 to KERNEL_WIDTH);
      Sum     : out slv32bit_array_t(0 to KERNEL_WIDTH)
  
    
      );
end component;
component Flag_Generator is
 Generic (
		constant IM_WIDTH  : positive :=IM_WIDTH

	);
	port(
        clk_i   : in  STD_LOGIC;
        valid_flag3 : in std_logic;
		Zero_flag : out boolean;
		flag_ari : out boolean;
		valid_out : out std_logic

		);
end component;
signal valid_flag2 : std_logic;
signal datak :       out_data(0 to 3);
-- signal datak1 :       out_data(0 to 3);
signal datas     :  slv16bit_array_t(0 to KERNEL_WIDTH);
signal valid     :  slv_array_t(0 to KERNEL_WIDTH-2);
signal wr_en   : std_logic:='1';
signal valid_flag : std_logic;
signal valid_flag1 : std_logic;
signal valid_flag3 : std_logic;
signal flag_ari : boolean;
signal Zero_Flag : boolean;
begin


  MAIN_RAM : Control port map(clk_i=>clk_i,rst_i=>rst_i,wr_en=>data_valid_i,data_i=>data_i,datas=>datas,valid=>valid);
  Flag_Gen : Flag_Generator port map(clk_i=>clk_i,valid_flag3=>valid_flag3,Zero_flag=>Zero_flag,flag_ari=>flag_ari,valid_out=>valid_out);
  BRAM : BRAM_one port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid(0),data_i=>datas(0),flag_left=>flag_ari,Zero_flag=>Zero_flag,data_out=>datak(0),kalo_flag=>valid_flag3);
  Gen_BRAM : for i in 1 to KERNEL_WIDTH-1 generate
     BRAM : BRAM1 port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid(0),data_i=>datas(i),flag_left=>flag_ari,data_out=>datak(i),kalo_flag=>valid_flag1);
  end generate;
  BRAMk: BRAM2 port map(clk_i=>clk_i,rst_i=>rst_i,data_valid_i=>valid(1),data_i=>datas(3),flag_left=>flag_ari,data_out=>datak(3));
  Convo : convo_unit port map(clk=>clk_i,c=>c,in_data=>datak,Sum=>Sum);
end Behavioral;
