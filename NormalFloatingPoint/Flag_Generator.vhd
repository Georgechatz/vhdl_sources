library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.mypack.all;
entity Flag_Generator is
    Generic (
		constant IM_WIDTH  : positive

	);
	port(
        clk_i   : in  STD_LOGIC;
        valid_in : in std_logic;
		Zero_flag : out boolean;
		k1 : out boolean;
		valid_out : out std_logic;
		valid_conv: out std_logic

		);
end Flag_Generator;


architecture beh of Flag_Generator is
signal count1: integer:=0;
signal count2: integer:=0;
signal count3: integer:=0;
signal count4: integer:=0;
signal count5: integer:=0;
signal count6: integer:=0;
signal flag5 : boolean;
signal flag6 : boolean:=false;
signal Zero_flag1 : boolean:=false;
signal flag4 : boolean:=false;
constant dn1: positive := 5;
signal delay1: std_ulogic_vector(0 to dn1 - 1);
signal valid_out1: std_logic;
constant dn: positive := 2;
signal delay: std_ulogic_vector(0 to dn - 1);
--signal valid_conv: std_logic;
signal flag_ari : boolean;
begin
proc_control : process(clk_i,count3)
  begin
  if rising_edge(clk_i) then
      count4<=count4+1;
      if count4= (2)*(IM_WIDTH)+9 then
         valid_out1<='1';
         flag5<=true;
      end if;
      if flag5 then
         count5<=count5+1;
         if count5 = IM_WIDTH then
            count6<=count6+1;
            count5<=0;
         end if;
      end if;
      if count6 = IM_WIDTH-1 then
         valid_out1<='0';
      elsif count6 = IM_WIDTH-3 and count1 = IM_WIDTH-1 then
         Zero_flag1<=true;
         
      end if;
      Zero_flag<=Zero_flag1;
     if count3 = 2*IM_WIDTH+3 then
         flag4<=true;
     end if;
     delay<=valid_in&delay(0 to dn-2);
     valid_conv<=delay(dn-1);
     delay1<=valid_out1&delay1(0 to dn1-2);
     valid_out<=delay1(dn1-1);
  end if;
  if falling_edge(clk_i) then
    count3<=count3+1;
    if flag4 then
    count1<=count1+1;
    end if;
   if count1 = IM_WIDTH-2 and flag4 then
       flag_ari<= true;
    elsif count1 = IM_WIDTH-1 and flag4 then
       flag_ari<=false;
       count1<=0;
       count2<=count2+1;
    else
      flag_ari<=false;
    end if;
   if count2 = IM_WIDTH then
       flag6<=true;
   end if;
   k1<=flag_ari;
   end if;
 end process;
 end beh;