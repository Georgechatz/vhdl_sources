library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;

entity conv_unit is
	 Generic (   
				 constant DATA_WIDTH 		: positive ;
				 constant KERNEL_WIDTH      : positive := 3
				 );
				 
    Port ( clk : in std_logic;
           data : in out_data (2 downto 0);
           B       : in out_data(2 downto 0);
           valid_in : in std_logic;
           Q : out  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0)
   
        
           );
end conv_unit;
architecture beh of conv_unit is

component Tree_ADD is
    generic( constant DATA_WIDTH :positive := DATA_WIDTH );
	port
	(   clk  : in std_logic;
	    data : in mult_data(2 downto 0);
		result : out std_logic_vector (DATA_WIDTH - 1 downto 0)
	);
	
end component;
component radix_4_multi is
  generic(n : integer :=16);
  port(clk : in std_logic;
       a   : in std_logic_vector(n-1 downto 0);
       b   : in std_logic_vector(n-1 downto 0);
       result : out std_logic_vector(2*n-1 downto 0)
 
     );
end component;
COMPONENT Float_Multiply
  PORT (
    aclk : IN STD_LOGIC;
    s_axis_a_tvalid : IN STD_LOGIC;
    s_axis_a_tready : OUT STD_LOGIC;
    s_axis_a_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_b_tvalid : IN STD_LOGIC;
    s_axis_b_tready : OUT STD_LOGIC;
    s_axis_b_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_result_tvalid : OUT STD_LOGIC;
    m_axis_result_tready : IN STD_LOGIC;
    m_axis_result_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

   signal multi : mult_data(2 downto 0);
   signal kati1,kati2,kati3 : bit_array(2 downto 0);
   
    type kernel_3x3 is array(0 to 2,0 to 2) of std_logic_vector(15 downto 0);
	--constant B : kernel_3x3 := (("1111111111110000","1111111111110000","1111111111110000"),
	            --              ("1111111111110000","0000000010000000","1111111111110000"),
	            --              ("1111111111110000","1111111111110000","1111111111110000"));
	                         --constant B : kernel_3x3 := (("1111111111110111","1111111111110001","1111111111110001"),
	                                              --       ("1111111111110100","1100101011011111","0110101101101010"),
	                                             --        ("0110101101101010","0110101101101010","0110101101101010"));
    begin
    
    Gen_Mul : for i in 0 to KERNEL_WIDTH-1 generate
	  Gen_Mul2 : for j in 0 to KERNEL_WIDTH-1 generate
	       --MULT : radix_4_multi port map(clk=>clk,a=>data(KERNEL_WIDTH-1-i)(j),b=>B(i)(j),result=> multi(i)(j));
	       FpMULT : Float_Multiply PORT MAP (   aclk => clk,
                                                s_axis_a_tvalid => '1',
                                                s_axis_a_tready => kati1(i)(j),
                                                s_axis_a_tdata => data(KERNEL_WIDTH-1-i)(j),
                                                s_axis_b_tvalid =>'1',
                                                s_axis_b_tready => kati2(i)(j),
                                                s_axis_b_tdata => B(i)(j),
                                                m_axis_result_tvalid => kati3(i)(j),
                                                m_axis_result_tready => '1',
                                                m_axis_result_tdata => multi(i)(j)
                                               );
	  end generate;
	end generate;
	TREE : Tree_ADD port map(clk=>clk,data=>multi,result=>Q);
	
	
		  


end beh;