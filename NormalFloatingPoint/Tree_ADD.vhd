library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_SIGNED.ALL;
library work;
use work.mypack.all;
entity Tree_ADD is
     Generic (
		
		constant DATA_WIDTH	: positive 
	);
	port
	(   clk: in std_logic;
		data : in mult_data(2 downto 0);
		result : out std_logic_vector (DATA_WIDTH - 1 downto 0)
	);
	
end entity;

architecture rtl of Tree_ADD is
COMPONENT ADDER
  PORT (
    aclk : IN STD_LOGIC;
    s_axis_a_tvalid : IN STD_LOGIC;
    s_axis_a_tready : OUT STD_LOGIC;
    s_axis_a_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_b_tvalid : IN STD_LOGIC;
    s_axis_b_tready : OUT STD_LOGIC;
    s_axis_b_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_result_tvalid : OUT STD_LOGIC;
    m_axis_result_tready : IN STD_LOGIC;
    m_axis_result_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;
	-- Declare registers to hold intermediate sums
signal sum1, sum2, sum3,sum4,buff1,sum5,sum6,buff2,sum7,buff3 : std_logic_vector (DATA_WIDTH-1 downto 0);
signal kati1,kati2,kati3 : slv_array_t(7 downto 0);
begin
    ADD_1: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(0), s_axis_a_tdata => data(0)(0),s_axis_b_tvalid => '1', s_axis_b_tready => kati2(0), s_axis_b_tdata => data(0)(1),m_axis_result_tvalid =>kati3(0), m_axis_result_tready => '1',  m_axis_result_tdata => sum1);
    ADD_2: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(1), s_axis_a_tdata => data(0)(2),s_axis_b_tvalid => '1', s_axis_b_tready => kati2(1), s_axis_b_tdata => data(1)(0),m_axis_result_tvalid =>kati3(1), m_axis_result_tready => '1',  m_axis_result_tdata => sum2);
    ADD_3: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(2), s_axis_a_tdata => data(1)(1),s_axis_b_tvalid => '1', s_axis_b_tready => kati2(2), s_axis_b_tdata => data(1)(2),m_axis_result_tvalid =>kati3(2), m_axis_result_tready => '1',  m_axis_result_tdata => sum3);
    ADD_4: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(3), s_axis_a_tdata => data(2)(0),s_axis_b_tvalid => '1', s_axis_b_tready => kati2(3), s_axis_b_tdata => data(2)(1),m_axis_result_tvalid =>kati3(3), m_axis_result_tready => '1',  m_axis_result_tdata => sum4);
    ADD_5: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(4), s_axis_a_tdata => sum1,s_axis_b_tvalid => '1', s_axis_b_tready => kati2(4), s_axis_b_tdata =>sum2,m_axis_result_tvalid =>kati3(4), m_axis_result_tready => '1',  m_axis_result_tdata => sum5);
	ADD_6: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(5), s_axis_a_tdata => sum3,s_axis_b_tvalid => '1', s_axis_b_tready => kati2(5), s_axis_b_tdata => sum4,m_axis_result_tvalid =>kati3(5), m_axis_result_tready => '1',  m_axis_result_tdata => sum6);
	ADD_7: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(6), s_axis_a_tdata => sum5,s_axis_b_tvalid => '1', s_axis_b_tready => kati2(6), s_axis_b_tdata =>sum6,m_axis_result_tvalid =>kati3(6), m_axis_result_tready => '1',  m_axis_result_tdata => sum7);
	ADD_8: ADDER port map(  aclk => clk, s_axis_a_tvalid => '1', s_axis_a_tready => kati1(7), s_axis_a_tdata => sum7,s_axis_b_tvalid => '1', s_axis_b_tready => kati2(7), s_axis_b_tdata =>buff3,m_axis_result_tvalid =>kati3(7), m_axis_result_tready => '1',  m_axis_result_tdata =>result);
	process(clk)
	
	   begin
	      if rising_edge(clk) then
		
			-- Generate and store intermediate values in the pipeline
			--sum1 <= data(0)(0)+data(0)(1);
			--sum2 <= data(0)(2)+data(1)(0);
			--sum3 <= data(1)(1)+data(1)(2);
			--sum4 <= data(2)(0)+data(2)(1);
			buff1<=data(2)(2);
			--sum5 <= sum1 +sum2;
			--sum6 <= sum3 +sum4;
			buff2<=buff1;
			--sum7 <= sum5 +sum6;
			buff3<=buff2;
			--result <= sum7 +buff3;
	    end if;
	
	   end process;
end rtl;