# VHDL_sources
VHDL Convolution Unit sources 

This directory contains:

***Basic Convolution Design***

**Fixed Point Components**
*  N-bit Fixed Point Convolution with Hard Multiplier (Vivado Default Using DSP)
*  N-bit Fixed Point Convolution with Radix-4 Multiplier (Prototype-No DSP use)
*  N-bit Fixed Point Convolution with Hybrid Radix-k Multiplier (k=6,8,10)

**Floating Point Components**
*  Floating Point 16-bit (half) Convolution using Vivado IPs for multiplication and addition
*  Floating Point 16-14-12 bit Convolution using custom components.
*  Block Floating Point convolution (One common exponent/Layer) 5-bit Exponent 8 or 10-bit Mantissa
*  "Smaller Block Floating Point" convolution (One common Exponent/(Output operation)) for 5 bit Exponent, 8 or 10-bit Mantissa

Basic Convolution Design for 3x3,5x5 and 7x7 Input Kernel Sizes. Design can be easily modified for different Kernel Sizes.

***WINOGRAD convolution Design***

3x3 Winograd Convolution for both floating and fixed point input images and filters.