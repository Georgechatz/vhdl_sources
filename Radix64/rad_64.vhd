library ieee;
use ieee.std_logic_1164.all;
library work;
use work.mypack.all;
entity rad_64 is
  generic(n : integer :=16);
  port(clk : in std_logic;
       a   : in std_logic_vector(n-1 downto 0);
       b   : in std_logic_vector(n-1 downto 0);
       result : out std_logic_vector(2*n-1 downto 0)
 
     );
end entity;
architecture rtl of rad_64 is
component rad_coding is
  generic( n : integer := 16);
  port ( clk : in std_logic;
         b : in std_logic_vector(n-1 downto 0);
         sign : out std_logic_vector(5 downto 0);
         thirtytwo : out std_logic;
         sixteen :   out std_logic;
         eight   :   out std_logic;
         four    :   out std_logic;
         two     :   out std_logic_vector(5 downto 0);
         one     :   out std_logic_vector(5 downto 0)
         );
end component;
component pp_r4 is
  generic(n : integer :=16);
  port(clk : in std_logic;
       a   : in std_logic_vector(n downto 0); -- a extended by one 2's complement number
       sign : in std_logic;
       one  : in std_logic;
       two  : in std_logic;
       pp_out : out std_logic_vector(n downto 0)
       );
end component;
component pp_r64 is 
   generic(n : integer := 16);

   port(clk : in std_logic;
        a : in std_logic_vector(n+5-1 downto 0); -- a extended by 5 bits (2's number)
        sign : in std_logic;
        thirtytwo : in std_logic;
        sixteen  : in std_logic;
        eight    : in std_logic;
        four     : in std_logic;
        pp_out   : out std_logic_vector(n+5-1 downto 0)
        );
end component;
component Correction_Terms is
   port(
      clk : in std_logic;
      sign : in std_logic_vector(5 downto 0);
      one  : in   std_logic_vector(5 downto 0);
      two  :  in std_logic_vector(5 downto 0);
      four : in std_logic;
      eight: in std_logic;
      sixteen: in std_logic;
      thirtytwo : in std_logic;
      Corr_Terms : out std_logic_vector(31 downto 0)
      );
end component;
component Adding_Unit is
     port(
         clk : in std_logic;
         pp_r64_out : in std_logic_vector(20 downto 0);
         pp_r4_out  : in slv17bit_array_t(4 downto 0);
         Corr_Terms : in std_logic_vector(31 downto 0);
         result     : out std_logic_vector(31 downto 0)
         );
end component;
signal thirtytwo,sixteen,eight,four : std_logic;
signal one,two,sign : std_logic_vector(5 downto 0);
signal a_r : std_logic_vector(n downto 0);
signal a_64 : std_logic_vector(n+5-1 downto 0);
signal pp_r4_out :  slv17bit_array_t(4 downto 0);
signal pp_r64_out : std_logic_vector(20 downto 0);
signal Corr_Terms:  std_logic_vector(31 downto 0);
begin
   process(clk)
     begin
      if rising_edge(clk) then
        a_r <= a(15)&a;
        a_64<= a(15)&a(15)&a(15)&a(15)&a(15)&a;
      end if;
     end process;
    
 




   RAD_CODE : rad_coding port map(clk=>clk,b=>b,sign=>sign,thirtytwo=>thirtytwo,sixteen=>sixteen,eight=>eight,four=>four,two=>two,one=>one);
   Cor_Terms: Correction_Terms port map(clk=>clk,sign=>sign,one=>one,two=>two,four=>four,eight=>eight,sixteen=>sixteen,thirtytwo=>thirtytwo,Corr_Terms=>Corr_Terms);
   Gen_pp_r4 : for i in 1 to (n/2)-3 generate
          pp_r4_gen: pp_r4 port map(clk=>clk,a=>a_r,sign=>sign(i),one=>one(i),two=>two(i),pp_out=>pp_r4_out(i-1));
   end generate;
   Gen_pp_r64 : pp_r64 port map(clk=>clk,a=>a_64,sign=>sign(0),thirtytwo=>thirtytwo,sixteen=>sixteen,eight=>eight,four=>four,pp_out=>pp_r64_out);
   ADD : Adding_Unit port map(clk=>clk,pp_r64_out=>pp_r64_out,pp_r4_out=>pp_r4_out,Corr_Terms=>Corr_Terms,result=>result);
end rtl;