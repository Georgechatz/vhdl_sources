library ieee;
use ieee.std_logic_1164.all;
package mypack is
    type slv17bit_array_t is array (natural range <>) of std_logic_vector(16 downto 0);
    type slv20bit_array_t is array (natural range <>) of std_logic_vector(20 downto 0);
    type slv32bit_array_t is array (natural range <>) of std_logic_vector(31 downto 0);
    type pp_17bit_data is array (natural range <>) of slv17bit_array_t(4 downto 0);
end package;

package body mypack is
end package body;