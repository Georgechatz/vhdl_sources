library ieee;
use ieee.std_logic_1164.all;
entity rad_coding is 
  generic( n : integer := 16);
  port ( clk : in std_logic;
         b : in std_logic_vector(n-1 downto 0);
         sign : out std_logic_vector(5 downto 0);
         thirtytwo : out std_logic;
         sixteen :   out std_logic;
         eight   :   out std_logic;
         four    :   out std_logic;
         two     :   out std_logic_vector(5 downto 0);
         one     :   out std_logic_vector(5 downto 0)
         );
 end entity;

    ----R4-----     ----R4----    ---R4---
 --   b15 b14 b13 b12 b11 b10 b9 b8 b7 b6 b5 b4 b3 b2 b1 b0
            ----R4-----     ---R4---    ----Approx_R64---
 --Approx_R64 -> {sign[0], thirtytwo, sixteen, eight, four}
 --LSB R4     -> {sign[1], two[1], one[1]}
 --
 -- MSB R4     -> {sign[5], two[5], one[5]}
 architecture rtl of rad_coding is
 begin
  process(clk)
   begin
   if rising_edge(clk) then
     sign(0)<=b(5);
     thirtytwo<=( not(b(5))and(b(4))and(b(3)))or((b(5))and(not(b(4)))and(not(b(3))));
     sixteen  <=((b(4))and(not(b(3)))and((not(b(5)))or(not(b(2)))))or((not(b(4)))and(b(3))and((b(5))or(b(2))));
     eight    <= ( not(b(5))and(not(b(4)))and(((not(b(3))and(b(2))and(b(1))))or(b(3)and(not(b(2))))))or( b(5)and(b(4))and((b(3)and(not(b(2)))and(not(b(1))))or((not(b(3)))and(b(2)))));
     four <=  (((not(b(4)))and(not(b(3)))and(not(b(2))))or(b(4)and(b(3))and(b(2))))and(b(2)xor(b(1)));
     two(0)<=  '0';
     one(0)<=  '0';
     for i in 3 to (n/2)-1 loop
       sign(i-2)<=b(2*i+1);
       two(i-2) <= ((not(b(2*i+1)))and(b(2*i))and(b(2*i-1)))or(b(2*i+1)and(not(b(2*i)))and(not(b(2*i-1))));
       one(i-2) <= b(2*i)xor(b(2*i-1));
     end loop;
   end if;
 
   end process;
 end rtl;    